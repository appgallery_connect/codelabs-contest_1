package com.droidzxy.secondclass;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.huawei.agconnect.appmessaging.AGConnectAppMessaging;
import com.huawei.agconnect.common.api.AGCInstanceID;
import com.huawei.agconnect.remoteconfig.AGConnectConfig;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = MainActivity.class.getSimpleName().toString();

    private AGConnectAppMessaging appMessaging;
    private WebView webView;
    private ImageView imgAd;

    private String imgurl;

    private String weburl;
    private String schoolurl;

    private Button btnHome;
    private Button btnSchool;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.wv_webview);
        imgAd = findViewById(R.id.img_ad);

        btnHome = findViewById(R.id.btn_home);
        btnSchool = findViewById(R.id.btn_school);

        btnHome.setOnClickListener(this);
        btnSchool.setOnClickListener(this);

        getAAID();
        getAppMessage();
        getRemoteConfig();
        initViews();
    }

    private void getAppMessage() {
        appMessaging = AGConnectAppMessaging.getInstance();
        appMessaging.setForceFetch();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void getRemoteConfig() {
        AGConnectConfig config = AGConnectConfig.getInstance();
        config.fetch(6).addOnSuccessListener(configValues -> {

            imgurl = config.getValueAsString("imgurl");
            weburl = config.getValueAsString("weburl");
            schoolurl = config.getValueAsString("schoolurl");

            config.apply(configValues);

            if (imgurl.startsWith("http://") || imgurl.startsWith("https://")) {
                URL url = null;
                try {
                    url = new URL(imgurl);
                    requestImg(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

            webView.loadUrl(weburl);

        }).addOnFailureListener(e -> {
            Toast.makeText(getBaseContext(), "Fetch Fail" + e.toString(), Toast.LENGTH_LONG).show();
        });
    }

    private void initViews() {
        webView.loadUrl("https://h5.zxx.edu.cn/");

        webView.getSettings().setJavaScriptEnabled(true);  //加上这一行网页为响应式的
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);//设置js可以直接打开窗口，如window.open()，默认为false
        webView.getSettings().setSupportZoom(false);//是否可以缩放，默认true
        webView.getSettings().setBuiltInZoomControls(true);//是否显示缩放按钮，默认false
        webView.getSettings().setUseWideViewPort(true);//设置此属性，可任意比例缩放。大视图模式
        webView.getSettings().setLoadWithOverviewMode(true);//和setUseWideViewPort(true)一起解决网页自适应问题
        webView.getSettings().setAppCacheEnabled(true);//是否使用缓存
        webView.getSettings().setDomStorageEnabled(true);//DOM Storage


        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                WebView.HitTestResult hit = view.getHitTestResult();

                if (url.startsWith("http://") || url.startsWith("https://")) { //加载的url是http/https协议地址
                    view.loadUrl(url);
                    return false; //返回false表示此url默认由系统处理,url未加载完成，会继续往下走

                } else { //加载的url是自定义协议地址
                    return true;
                }
            }
        });




    }

    public void getAAID() {
        String aaid = AGCInstanceID.getInstance(getBaseContext()).getId();
        Log.d("TAG", "getAAID successfully, aaid is " + aaid );
    }


    private void requestImg(final URL imgUrl)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = null;
                try {

                    bitmap = BitmapFactory.decodeStream(imgUrl.openStream());

                    showImg(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void showImg(final Bitmap bitmap){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgAd.setImageBitmap(bitmap);
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_home:
                if (weburl.startsWith("http://") || weburl.startsWith("https://")) {
                    webView.loadUrl(weburl);
                }
                break;
            case R.id.btn_school:
                if (schoolurl.startsWith("http://") || schoolurl.startsWith("https://")) {
                    webView.loadUrl(schoolurl);
                }
                break;
        }
    }
}