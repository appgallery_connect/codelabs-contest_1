package com.example.codechallengesdemo.slice;

import com.example.codechallengesdemo.ResourceTable;
import com.huawei.agconnect.appmessaging.AGConnectAppMessaging;
import com.huawei.agconnect.appmessaging.AGConnectAppMessagingOnClickListener;
import com.huawei.agconnect.appmessaging.model.Action;
import com.huawei.agconnect.appmessaging.model.AppMessage;
import com.huawei.agconnect.common.api.AGCInstanceID;
import com.huawei.agconnect.remoteconfig.AGConnectConfig;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.audio.*;
import ohos.media.codec.Codec;
import ohos.media.common.BufferInfo;
import ohos.media.common.Source;

import java.nio.ByteBuffer;

public class MainAbilitySlice extends AbilitySlice {
    private AGConnectAppMessaging appMessaging;

    private static final String TAG = MainAbilitySlice.class.getName();

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, TAG);

    private static final int SAMPLE_RATE = 44100;

    private static final int BUFFER_SIZE = 1024;

    private AudioRenderer audioRenderer;

    private Codec codec;

    long interval = 0;

    private String flag = "late";
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        // 初始化音频播放相关参数
        initAudioRenderer();

        // 获取AAID
        getAAID();

        //配置应用内消息
        configAppMessage();

        // 读取远程配置
        readRemoteConfig();


    }

    private void initAudioRenderer() {
        AudioStreamInfo audioStreamInfo = new AudioStreamInfo.Builder().sampleRate(SAMPLE_RATE)
                .audioStreamFlag(AudioStreamInfo.AudioStreamFlag.AUDIO_STREAM_FLAG_MAY_DUCK)
                .encodingFormat(AudioStreamInfo.EncodingFormat.ENCODING_PCM_16BIT)
                .channelMask(AudioStreamInfo.ChannelMask.CHANNEL_OUT_STEREO)
                .streamUsage(AudioStreamInfo.StreamUsage.STREAM_USAGE_MEDIA)
                .build();
        AudioRendererInfo audioRendererInfo = new AudioRendererInfo.Builder().audioStreamInfo(audioStreamInfo)
                .audioStreamOutputFlag(AudioRendererInfo.AudioStreamOutputFlag.AUDIO_STREAM_OUTPUT_FLAG_DIRECT_PCM)
                .bufferSizeInBytes(BUFFER_SIZE)
                .isOffload(false)
                .sessionID(AudioRendererInfo.SESSION_ID_UNSPECIFIED)
                .build();
        audioRenderer = new AudioRenderer(audioRendererInfo, AudioRenderer.PlayMode.MODE_STREAM);
        audioRenderer.setVolume(1.0f);
        audioRenderer.setSpeed(1.0f);
        setPlayCallback(audioStreamInfo);
    }

    private void setPlayCallback(AudioStreamInfo streamInfo) {
        AudioInterrupt audioInterrupt = new AudioInterrupt();
        AudioManager audioManager = new AudioManager();
        audioInterrupt.setStreamInfo(streamInfo);
        audioInterrupt.setInterruptListener((type, hint) -> {
            if (type == AudioInterrupt.INTERRUPT_TYPE_BEGIN && hint == AudioInterrupt.INTERRUPT_HINT_PAUSE) {
                HiLog.info(LABEL_LOG, "%{public}s", "sound paused");
            } else if (type == AudioInterrupt.INTERRUPT_TYPE_BEGIN && hint == AudioInterrupt.INTERRUPT_HINT_STOP) {
                HiLog.info(LABEL_LOG, "%{public}s", "sound stopped");
            } else if (type == AudioInterrupt.INTERRUPT_TYPE_END && (hint == AudioInterrupt.INTERRUPT_HINT_RESUME)) {
                HiLog.info(LABEL_LOG, "%{public}s", "sound resumed");
            } else {
                HiLog.info(LABEL_LOG, "%{public}s", "unknown state");
            }
        });
        audioManager.activateAudioInterrupt(audioInterrupt);
    }

    /**
     * 获取AAID
     */
    private void getAAID() {
        // 获取手机aaid, 添加在AGC上
        String aaid = AGCInstanceID.getInstance().getId();
        HiLog.info(LABEL_LOG,"手机调试AAID: " + aaid);
    }

    /**
     * 配置应用内消息
     */
    private void configAppMessage() {
        appMessaging = AGConnectAppMessaging.getInstance();
        // 设置是否允许同步AGC服务端数据
        appMessaging.setFetchMessageEnable(true);
        // 强制请求AGC服务端消息数据
        appMessaging.setForceFetch("AppOnForeground");
        // 设置是否允许展示消息
        appMessaging.setDisplayEnable(true);
        // 消息监听器
        MainAbilitySlice.ClickListener listener = new ClickListener();
        appMessaging.addOnClickListener(listener);
    }

    public static class ClickListener implements AGConnectAppMessagingOnClickListener {
        @Override
        public void onMessageClick(AppMessage appMessage, Action action) {
            HiLog.info(LABEL_LOG,"onMessageClick Success");
        }
    }

    /**
     * 读取远程配置
     */
    private void readRemoteConfig() {
        AGConnectConfig aGConnectConfig = AGConnectConfig.getInstance();
        aGConnectConfig.fetch(interval)
                .addOnSuccessListener(configValues -> {
                    HiLog.info(LABEL_LOG,"Fetch Success");
                    aGConnectConfig.apply(configValues);
                    // 获取远程配置
                    String value = aGConnectConfig.getValueAsString("background_image");
                    HiLog.info(LABEL_LOG,"background_image:"+ value);
                    Image image = (Image) findComponentById(ResourceTable.Id_imgHeader);
                    if (value.equals(flag)) {
                        image.setPixelMap(ResourceTable.Media_second);
                        // 播放音频
                        audioRenderer.start();
                        decoderAudio();
                    }else {
                        image.setPixelMap(ResourceTable.Media_first);
                    }

                }).addOnFailureListener(e -> {
                    HiLog.error(LABEL_LOG,"Fetch Fail");
                });
    }

    private void decoderAudio() {
        if (codec == null) {
            codec = Codec.createDecoder();
        } else {
            codec.stop();
        }

        Source source = new Source(getFilesDir() + "/sample.mp3");
        codec.setSource(source, null);
        codec.registerCodecListener(new Codec.ICodecListener() {
            @Override
            public void onReadBuffer(ByteBuffer outputBuffer, BufferInfo bufferInfo, int trackId) {
                audioRenderer.write(outputBuffer, bufferInfo.size);
            }

            @Override
            public void onError(int errorCode, int act, int trackId) {
                HiLog.error(LABEL_LOG, "%{public}s", "codec registerCodecListener error");
            }
        });
        codec.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (codec != null) {
            codec.stop();
        }
        if (audioRenderer != null) {
            audioRenderer.release();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
