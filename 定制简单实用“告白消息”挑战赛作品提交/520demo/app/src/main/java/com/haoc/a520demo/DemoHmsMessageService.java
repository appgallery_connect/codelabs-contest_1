package com.haoc.a520demo;

import android.content.SharedPreferences;
import android.util.Log;

import com.huawei.hms.push.HmsMessageService;
import com.huawei.hms.push.RemoteMessage;

/**
 * @description: HmsMessageService
 * @author: Haoc
 * @Time: 2022/5/16  20:11
 */

public class DemoHmsMessageService extends HmsMessageService {
    private final static String TAG = "DemoHmsMessageService";

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Log.i(TAG, "onMessageReceived is called");
        if (message.getDataOfMap().containsKey("DATA_STATE")) {
            SharedPreferences preferences = this.getApplicationContext().getSharedPreferences("Remote_Config", MODE_PRIVATE);
            preferences.edit().putBoolean("DATA_OLD", true).apply();
        }
    }
}
