### **项目简介**

本应用基于安卓平台实现了简单告白消息，集成了应用内消息，远程配置，App Linking，云存储以及云函数等服务

|                   业务功能                   |  技术选型   |                           选项来源                           |
| :------------------------------------------: | :---------: | :----------------------------------------------------------: |
|              营销消息定制及展示              | 应用内消息  | [AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
|              “5.20”节日主题更新              |  远程配置   | [AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
|              应用内消息图片存储              |   云存储    | [AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
| 应用内消息深度链接；生成上传图片链接用于分享 | App Linking | [AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
|                 触发远程配置                 |   云函数    | [AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |

### **开发准备**

#### **1. App Linking**

添加网址允许清单

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521155944.49885677409783171399075875625700:50530520164225:2800:2C344EB07EDFBA3D5E4FFA5F0A548CD89652A4E8BD1F0A4ECEDB2E3416E2CFEF.png)

创建App Linking

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521160045.58639506423334374890275628783940:50530520164225:2800:D72CDAE43E4D8E755D60D651D8158EE38CAE8C127427D35986DA9A0BBCB1BD12.png)

#### **2. 云存储**

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521160114.34798232691905817485518194171400:50530520164225:2800:8AA3421BB325E9CF5835010FC15ACC53BB810B4455450AA4AE4C9510CC6B140D.png)

#### **3. 应用内消息**

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521160143.21729662627608978050595833527466:50530520164225:2800:45895366950D2E42FDF5169C9A6AF3FB07465E4D3003117156DFD6CE55922781.png)

#### **4. 远程配置**

添加配置项

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521160212.70403955148999761336056841267473:50530520164225:2800:02DB14B641F4CC172F9498D902D5D93C65C8279D2D4B32355107FF63C629049B.png)

添加配置条件

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521160239.07535290871481254684318007762813:50530520164225:2800:66517DF8F2410DBD7D00CB80601EDE7B119A1F8E43DB7126CE5CAA8CA6960BE0.png)

![img](https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521160300.70803202624235989403562679417734:50530520164225:2800:6D1D902286D6006D6740D5A1FDEB5E826BCF8E3B8160D44579827675AC5DE9F5.png)

#### **5. 云函数**

参见[实时更新远程配置参数-Android-远程配置 (huawei.com)](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-Guides/agc-remoteconfig-android-usingfunction-0000001056347167)

### **关键代码**

匿名登录

```java
public void login() {
    AGConnectAuth auth = AGConnectAuth.getInstance();
    auth.signInAnonymously().addOnSuccessListener(signInResult -> {
        Log.i(TAG, "Sign in for agc success ");
    }).addOnFailureListener(e -> {
        Log.w(TAG, "Sign in for agc failed: " + e.getMessage());
    });
}
```

创建App Linking

```java
private static final String DOMAIN_URI_PREFIX = "{App Linking申请的链接前缀}";
```

```java
private void createAppLinking() {
    AppLinking.Builder builder = AppLinking.newBuilder()
            .setUriPrefix(DOMAIN_URI_PREFIX)
            .setDeepLink(Uri.parse(uri))
            .setAndroidLinkInfo(AppLinking.AndroidLinkInfo.newBuilder()
                    .setAndroidDeepLink(uri)
                    .build());
    builder.buildShortAppLinking().addOnSuccessListener(shortAppLinking -> {
        Uri shortLinkUri = shortAppLinking.getShortUrl();
        App_LinkingUrl = shortLinkUri.toString();
    }).addOnFailureListener(e -> {
        //AppLinkingException
        Log.i("failed", "" + e.getMessage());
    });
}
```

上传图片至云存储

```java
private void uploadFile(File file) {
    final String path = "photo.jpg";
    if (!file.exists()) {
        Log.w(TAG, "file is not exist! ");
        return;
    }
    if (mAGCStorageManagement == null) {
        initAGCStorageManagement();
    }
    StorageReference storageReference = mAGCStorageManagement.getStorageReference(path);
    UploadTask uploadTask = (UploadTask) storageReference.putFile(file)
            .addOnSuccessListener(new OnSuccessListener<UploadTask.UploadResult>() {
                @Override
                public void onSuccess(UploadTask.UploadResult uploadResult) {
                    Toast.makeText(getBaseContext(), "上传成功! ", Toast.LENGTH_SHORT).show();
                    getDownLoadUrl();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(getBaseContext(), "上传失败 " + e.getMessage(), Toast.LENGTH_LONG).show();
                    Log.w(TAG, "upload failure! " + e.getMessage());
                }
            });
}
```

获取上传图片的链接

```java
public void getDownLoadUrl() {
    if (mAGCStorageManagement == null) {
        initAGCStorageManagement();
    }
    StorageReference storageReference = mAGCStorageManagement.getStorageReference("photo.jpg");
    Task<Uri> task = storageReference.getDownloadUrl();
    task.addOnSuccessListener(new OnSuccessListener<Uri>() {
        @Override
        public void onSuccess(Uri uri) {
            String downloadUrl = uri.toString();
            Intent intent = new Intent(MainActivity.this, PhotoActivity.class);
            intent.putExtra("Url", downloadUrl);
            startActivity(intent);
        }
    });
    task.addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(Exception e) {
            Log.e(TAG, "get download url failed: " + e.getMessage());
        }
    });
}
```

远程配置设置主题

```java
private void setTheme() {
    SharedPreferences preferences = this.getApplicationContext().getSharedPreferences("Remote_Config", MODE_PRIVATE);
    // 获取数据的间隔时间，单位是秒
    long fetchInterval = 12 * 60 * 60L;
    if (preferences.getBoolean("DATA_OLD", false)) {
        fetchInterval = 0;
    }
    // 使用AGConnectConfig.fetch接口获取远程配置参数值，并为方法添加addOnSuccessListener监听器
    config.fetch(fetchInterval).addOnSuccessListener(configValues -> {
        // Make the configuration parameters take effect
        config.apply(configValues);

        //以String方式获取参数值
        String value = config.getValueAsString("theme_color");
        Log.i("theme_color", "" + value);

        if (value.equals(flag)) {
            // 加载云存储上的图片
            Picasso.with(MainActivity.this)
                    .load("https://agc-storage-drcn.platform.dbankcloud.cn/v0/520-ee2ry/image_detail_background_change.jpeg?token=591703b4-145e-4bf9-a3d2-f67a1006cb59")
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(img_banner);
        }
    }).addOnFailureListener(e -> {
        Toast.makeText(getBaseContext(), "Fetch Fail", Toast.LENGTH_SHORT).show();
    });
    Log.i("MessageDisplay", "display message success");
}
```

### **演示效果**

模拟到达活动时间后通过远程配置设置Banner图片以及弹出应用内消息

<img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521163213.95607597735796612662525992544130:50530520164225:2800:AFAD5B7E0279CC9FE690E96C083E2984FE34484D6EE416E47B5EFAEBF65FD5CC.gif" alt="img" style="zoom:50%;" />

点击前往选购跳转到App Linking设置的链接

<img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220530105005.88135476547266904551829298863135:50530529034658:2800:25A0504C7533FFD203DF7AEBF2F7674B08F53F4278252E146A8BB5C2F8CD0F67.gif" alt="img" style="zoom:50%;" />

模拟用户上传图片并通过社交软件分享给好友

<img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521162124.71504561814404199731111915747776:50530520164225:2800:C021685206C83E084838430FE22A1554ABEA43198E025F7F13645E8FC2854F88.gif" alt="img" style="zoom:50%;" />

好友点击链接可查看分享的图片

<img src="https://alliance-communityfile-drcn.dbankcdn.com/FileServer/getFile/cmtybbs/251/810/102/0260086000251810102.20220521162231.81048076821122740866253160626802:50530520164225:2800:38F5F20D66AF6384B7650026465D300C9DC98DF3B4A0F985F76DF48D23BA7C10.gif" alt="img" style="zoom:50%;" />
