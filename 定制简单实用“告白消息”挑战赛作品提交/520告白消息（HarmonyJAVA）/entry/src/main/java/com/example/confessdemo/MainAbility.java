package com.example.confessdemo;

import com.example.confessdemo.slice.MainAbilitySlice;
import com.huawei.agconnect.AGConnectInstance;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;


public class MainAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        AGConnectInstance.initialize(getAbilityPackage());
    }
}
