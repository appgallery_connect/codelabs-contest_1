## 【Codelabs挑战赛—定制消息和配置】Android版定制520简易告白消息

### 一、效果展示

![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529112529.15479075683338172726816717241303:20220529123410:2800:F034C9E4F116B1BE37BE92F5440F56FA7E115BD662181EF51046933BE02D9C97.gif)



### 二、选用技术

|      业务功能      |   技术选型    |                           技术来源                           |
| :----------------: | :-----------: | :----------------------------------------------------------: |
| 营销消息定制及展示 |  应用内消息   | [AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
| 5.20”节日主题更新  |   远程配置    | [ AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
| 应用内消息图片存储 | 云存储+云托管 | [ AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |
| Applinking链接跳转 |  Applinking   | [ AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/) |

### 三、前期准备
####  1 云存储+云托管配置
*  云存储
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529114021.47470525777916984443966194016970:20220529123410:2800:7DA62CA91F9657699C1DA33067BC4C64CF8FFA6CCF5C4E6B140ED9C5F86BDB9E.png)
* 云托管
* ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529114038.96613101737588397056111886796087:20220529123410:2800:D5F0562057A5128295005D021446436B33451E8F7F50CC5A7AB3B8083219DE65.png)
    *  **注意：**远程配置需要有域名，具体可看[官方文档](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-Guides/agc-cloudhosting-introductions-0000001057944575)

####  2. Applink配置
*  Applink
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529114306.16883747389189171131294687779710:20220529123410:2800:C28CC18E06CAC69394F910DF2F1FFC28905E33F114A51B62830D04EE557EB71B.png)
* **出现问题（未解决）**
    *  1. 链接前缀配置完成
    *  2. Applink配置完成
    *  3. 网站允许清单配置完成
    *  在调测的时候出现问题（出现502网关问题）
    *  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529114929.03257420617717325217574163781635:20220529123410:2800:E08A0026CC18292E9694C22C7757431A58749FB44944488ED8A6521A8180503F.png)

#### 3. 远程配置
*  远程配置
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529115128.23886139912063582060612779566016:20220529123410:2800:D1370476EFF1A7E6AB24E81408CB5F4B118C5B11B3E4117422F1C8F78D2FC439.png)
    * 其实只要配置默认与时间晚于就行（时间晚于是指时间晚于5月20号00:00:00在UTC8:00时区下）

#### 4. 应用内消息配置
*  展示
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529115758.25194202582915811635834381903837:20220529123410:2800:7C5DF0FD3D3FB87E0EFAEEDF9EE60AF3914F7D435BF000847901A93C91C1557F.png)
*  部分配置
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529115818.11204665840204853734635991006518:20220529123410:2800:BE3CCFEB71A91A808223E8B73B4D2438F482557C48AC03D527DE4FFC0EB34847.png)
*  具体可参考[官方文档](https://developer.huawei.com/consumer/cn/doc/development/AppGallery-connect-Guides/agc-appmessage-introduction-0000001071884501)

### 四、代码编写
#### 1. 目录结构
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529122312.47639900164421617193570230375491:20220529123410:2800:A993B6A54037BDACBAEC2D0C923EDA80B916608B07A7420B1F9D558511AB991F.png)

#### 2. 配置agconnect-services.json文件
*  进入AGC平台，在你的具体项目首页，下载最新的agconnect-services.json文件
*  将下载好的文件拷贝到AndroidStudio项目的app目录下（以project的方式查看项目结构）
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529120840.14928819922960950807176871460205:20220529123410:2800:C05008242CE75804F1DAB92C00E63F61C677D1E1F586217FA50BE7ABD1782CFC.png)

#### 3. 配置仓库，引入依赖
*  配置仓库（配置项目级build.gradle）(具体更改参考自己项目)
* ```json
  // Top-level build file where you can add configuration options common to all sub-projects/modules.
  buildscript {
      repositories {
          google()
          jcenter()
          //maven仓地址
          maven {url 'https://developer.huawei.com/repo/'}
      }
      dependencies {
          classpath "com.android.tools.build:gradle:4.1.0"
          //AGC插件地址
          classpath 'com.huawei.agconnect:agcp:1.6.5.300'
          // NOTE: Do not place your application dependencies here; they belong
          // in the individual module build.gradle files
      }
  }
  
  
  allprojects {
      repositories {
          google()
          jcenter()
          //maven仓地址
          maven {url 'https://developer.huawei.com/repo/'}
      }
  }
  
  task clean(type: Delete) {
      delete rootProject.buildDir
  }
  ```
*  引入依赖（配置应用级build.gradle）(具体更改参考自己项目)
* ```json
  plugins {
      id 'com.android.application'
      //添加AGC的依赖
      id 'com.huawei.agconnect'
  }
  
  android {
      compileSdk 31
  
      defaultConfig {
          applicationId "com.xray23.demo520"
          minSdk 21
          targetSdk 31
          versionCode 1
          versionName "1.0"
  
          testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
      }
  
      buildTypes {
          release {
              minifyEnabled false
              proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
          }
      }
      compileOptions {
          sourceCompatibility JavaVersion.VERSION_1_8
          targetCompatibility JavaVersion.VERSION_1_8
      }
  }
  
  dependencies {
  
      implementation 'androidx.appcompat:appcompat:1.2.0'
      implementation 'com.google.android.material:material:1.3.0'
      implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
      testImplementation 'junit:junit:4.+'
      androidTestImplementation 'androidx.test.ext:junit:1.1.2'
      androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
      // glide图片加载框架
      implementation group: 'com.github.bumptech.glide', name: 'glide', version: '4.8.0'
      //agc-core的依赖
      implementation 'com.huawei.agconnect:agconnect-core:1.6.5.300'
      //应用内消息sdk
      implementation 'com.huawei.agconnect:agconnect-appmessaging:1.6.5.300'
      //远程配置sdk
      implementation 'com.huawei.agconnect:agconnect-remoteconfig:1.6.5.300'
      //华为分析SDK，查看消息展示报表需要
      implementation 'com.huawei.hms:hianalytics:6.4.1.302'
  }
  ```

#### 4. 代码编写
*  activity_main.xml
* ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
      xmlns:tools="http://schemas.android.com/tools"
      android:layout_width="match_parent"
      android:layout_height="match_parent"
      android:orientation="vertical"
      tools:context=".MainActivity">
  
      <TextView
          android:layout_width="match_parent"
          android:layout_height="wrap_content"
          android:layout_marginTop="10dp"
          android:layout_marginBottom="10dp"
          android:gravity="center"
          android:text="新图片"
          android:textColor="@color/black"
          android:textSize="24dp" />
  
      <ImageView
          android:id="@+id/img_view"
          android:layout_width="match_parent"
          android:layout_height="wrap_content"
          android:adjustViewBounds="true"
          android:layout_marginBottom="20dp"
          android:src="@mipmap/banner" />
  
      <TextView
          android:layout_width="match_parent"
          android:layout_height="wrap_content"
          android:layout_marginTop="10dp"
          android:layout_marginBottom="10dp"
          android:gravity="center"
          android:text="原来图片"
          android:textColor="@color/black"
          android:textSize="24dp" />
  
      <ImageView
          android:layout_width="match_parent"
          android:layout_height="wrap_content"
          android:adjustViewBounds="true"
          android:src="@mipmap/banner" />
  
  </LinearLayout>
  ```
*  MainActivity.java
* ```java
  package com.xray23.demo520;
  
  import androidx.appcompat.app.AppCompatActivity;
  
  import android.content.Intent;
  import android.graphics.drawable.Drawable;
  import android.net.Uri;
  import android.os.Bundle;
  import android.util.Log;
  import android.widget.ImageView;
  import android.widget.Toast;
  
  
  import com.bumptech.glide.Glide;
  import com.huawei.agconnect.appmessaging.AGConnectAppMessaging;
  import com.huawei.agconnect.appmessaging.model.Action;
  import com.huawei.agconnect.appmessaging.model.AppMessage;
  import com.huawei.agconnect.remoteconfig.AGConnectConfig;
  import com.huawei.hmf.tasks.OnFailureListener;
  import com.huawei.hmf.tasks.OnSuccessListener;
  import com.huawei.hmf.tasks.Task;
  import com.huawei.hms.aaid.HmsInstanceId;
  import com.huawei.hms.aaid.entity.AAIDResult;
  
  import java.io.InputStream;
  import java.net.URL;
  
  public class MainActivity extends AppCompatActivity {
  
      public static final String TAG = "demo520消息";
  
      // 默认远程配置
      // key
      private static final String REMOTE_KEY = "banner_img";
      // value
      private static final String REMOTE_VALUE = "new_banner";
      // 调用远程配置更新的间隔
      private long fetchInterval = 0;
  
      private AGConnectAppMessaging appMessaging;
      private AGConnectConfig appConfig;
  
      // banner图片
      private ImageView imageView;
      //云存储中banner图片url地址
      private static final String URL = "https://blog.tengfei.link/inAppMsg/1653745181158/520.png";
  
      @Override
      protected void onCreate(Bundle savedInstanceState) {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_main);
  
          // 获取ImageView对象
          imageView = findViewById(R.id.img_view);
  
          // 获取aaid
          getAAID();
          // 应用内消息
          configMessage();
          // 远程配置
          configRemote();
  
      }
  
      private void configRemote() {
          // 实例化
          appConfig = AGConnectConfig.getInstance();
  
          appConfig.fetch(fetchInterval).addOnSuccessListener(configValues -> {
              // 配置参数生效
              appConfig.apply(configValues);
  
              //以String方式获取参数值
              String value = appConfig.getValueAsString(REMOTE_KEY);
  
              if (value.equals(REMOTE_VALUE)) {
                  // 使用Glide图片框架更改图片（需要在AndroidManifest.xml中加入网络访问权限）
                  Glide.with(MainActivity.this).load(URL).into(imageView);
              }
  
          }).addOnFailureListener(e -> {
              Toast.makeText(getBaseContext(), "Fetch Fail", Toast.LENGTH_LONG).show();
          });
      }
  
      /**
       * 配置应用内消息
       */
      private void configMessage() {
          // 实例化
          appMessaging = AGConnectAppMessaging.getInstance();
          // 设置是否允许同步AGC服务端数据
          appMessaging.setFetchMessageEnable(true);
          // 强制请求AGC服务端消息数据
          appMessaging.setForceFetch("AppOnForeground");
          // 设置是否允许展示消息
          appMessaging.setDisplayEnable(true);
          // 消息展示监听器
          appMessaging.addOnDisplayListener((appMessage) -> {
                      System.out.println("展示消息成功");
                      Toast.makeText(MainActivity.this, "展示消息成功", Toast.LENGTH_SHORT).show();
                  }
          );
          // 消息点击监听器
          appMessaging.addOnClickListener((AppMessage appMessage, Action action) -> {
                      //点击后打开弹框消息设置的url
                      String urlStr = action.getActionUrl();
                      Log.i(TAG, "getActionUrl: card url" + urlStr);
                      Uri url = Uri.parse(urlStr);
                      Log.i(TAG, "onMessageClick: message clicked" + url);
                      Intent intent = new Intent(Intent.ACTION_VIEW);
                      intent.addCategory(Intent.CATEGORY_BROWSABLE);
                      intent.setData(url);
                      startActivity(intent);
                  }
          );
      }
  
      /**
       * 获取AAID
       */
      private void getAAID() {
          Task<AAIDResult> idResult = HmsInstanceId.getInstance(getApplicationContext()).getAAID();
          idResult.addOnSuccessListener(new OnSuccessListener<AAIDResult>() {
              @Override
              public void onSuccess(AAIDResult aaidResult) {
                  // 获取AAID方法成功
                  String aaid = aaidResult.getId();
                  Log.d(TAG, "getAAID successfully, aaid is " + aaid);
              }
          }).addOnFailureListener(new OnFailureListener() {
              @Override
              public void onFailure(Exception myException) {
                  // 获取AAID失败
                  Log.d(TAG, "getAAID failed, catch exception: " + myException);
              }
          });
      }
  
      /**
       * 加载imgUrl
       *
       * @param url
       * @return
       */
      private void loadImageFromUrl(String url) {
          new Thread(() -> {
              try {
                  InputStream is = (InputStream) new URL(url).getContent();
                  Drawable d = Drawable.createFromStream(is, "banner图片地址");
                  imageView.setImageDrawable(d);
              } catch (Exception e) {
                  System.out.println("Exc=" + e);
              }
          }).start();
  
      }
  }
  ```
*  **在AGC平添应用内消息中找到调测，将获取到的AAID填入到测试用户AAID中**


*  AndroidManifest.xml
* ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <manifest xmlns:android="http://schemas.android.com/apk/res/android"
      package="com.xray23.demo520">
  
      <uses-permission android:name="android.permission.INTERNET" /> <!-- 网络访问权限 -->
  
      <application
          android:allowBackup="true"
          android:icon="@mipmap/ic_launcher"
          android:label="@string/app_name"
          android:roundIcon="@mipmap/ic_launcher_round"
          android:supportsRtl="true"
          android:theme="@style/Theme.Demo520">
          <activity
              android:name=".MainActivity"
              android:exported="true">
              <intent-filter>
                  <action android:name="android.intent.action.MAIN" />
  
                  <category android:name="android.intent.category.LAUNCHER" />
              </intent-filter>
          </activity>
      </application>
  
  </manifest>
  ```
*  **最后运行即可**

### 五、展示
*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529123339.37766792950528165400884317330100:20220529123410:2800:86E5060DA03EB18AF82DDE68C9B4852E310AF4895DB9F343D6944588928B4412.jpg)

*  ![](https://communityfile-drcn.op.hicloud.com/FileServer/getFile/cmtybbs/008/300/441/0000000000008300441.20220529112529.15479075683338172726816717241303:20220529123410:2800:F034C9E4F116B1BE37BE92F5440F56FA7E115BD662181EF51046933BE02D9C97.gif)

