package com.droidzxy.membership;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import model.memberInfo;

public class MemberAdapter  extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {

    public static class MemberItem implements Comparable<MemberItem>{

        public int id;
        public String name;
        public int level;
        public int points;

        public MemberItem() {
        }

        public MemberItem(int id, String name, int level, int points) {

            this.id = id;
            this.name = name;
            this.level = level;
            this.points = points;
        }

        @Override
        public String toString() {
            return id + ","+ name;
        }

        // sort From big to small
        @Override
        public int compareTo(MemberItem o) {
            if(this.id < o.id) {
                return -1;
            } else if(this.id == o.id) {
                return 0;
            } else {
                return 1;
            }
        }
    }


    private final ArrayList<MemberItem> mList;
    private MainActivity.UiCallBack mCallBack;

    public MemberAdapter(ArrayList<MemberItem> list, MainActivity.UiCallBack callBack) {
        mList = list;
        mCallBack = callBack;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtId.setText(String.valueOf(position + 1));
        holder.txtCard.setText(String.valueOf(mList.get(position).id));
        holder.txtName.setText(String.valueOf(mList.get(position).name));
        holder.txtLevel.setText(String.valueOf(mList.get(position).level));
        holder.txtPoints.setText(String.valueOf(mList.get(position).points));

        int pos = position;
        holder.imgDel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                memberInfo info = new memberInfo();
                info.setId(mList.get(pos).id);
                info.setName(String.valueOf(mList.get(pos).name));
                info.setLevel(mList.get(pos).level);
                info.setPoints(mList.get(pos).points);

                mCallBack.onDelete(info);
            }
        });

        holder.imgModify.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                memberInfo info = new memberInfo();
                info.setId(mList.get(pos).id);
                info.setName(String.valueOf(mList.get(pos).name));
                info.setLevel(mList.get(pos).level);
                info.setPoints(mList.get(pos).points);

                mCallBack.onModify(info);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout layoutItem;
        public final TextView txtId;
        public final TextView txtCard;
        public final TextView txtName;
        public final TextView txtLevel;
        public final TextView txtPoints;
        public final ImageView imgModify;
        public final ImageView imgDel;

        public ViewHolder(View view) {
            super(view);
            layoutItem = (LinearLayout)view.findViewById(R.id.layout_item);
            txtId = (TextView) view.findViewById(R.id.txt_item_id);
            txtCard = (TextView) view.findViewById(R.id.txt_item_card);
            txtName = (TextView) view.findViewById(R.id.txt_item_name);
            txtLevel = (TextView) view.findViewById(R.id.txt_item_level);
            txtPoints = (TextView) view.findViewById(R.id.txt_item_points);
            imgModify = (ImageView)view.findViewById(R.id.img_modify);
            imgDel = (ImageView)view.findViewById(R.id.img_del);
        }

    }
}
