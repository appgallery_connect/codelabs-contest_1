package utils;
import com.bin.david.form.annotation.SmartColumn;
import com.bin.david.form.annotation.SmartTable;

@SmartTable(name = "体检结果记录表")
public class PersonInfo {
    public PersonInfo(int pesonId, String name, int age, String hasInoculate, String cardNo) {
        this.pesonId = pesonId;
        this.name = name;
        this.age = age;
        this.hasInoculate = hasInoculate;
        this.cardNo = cardNo;
    }

    @SmartColumn(id = 0, name = "序号")
    private int pesonId;
    @SmartColumn(id = 1, name = "姓名")
    private String name;
    @SmartColumn(id = 2, name = "年龄")
    private int age;
    @SmartColumn(id = 3, name = "是否已体检")
    private String hasInoculate;
    @SmartColumn(id = 4, name = "证件号")
    private String cardNo;
}
