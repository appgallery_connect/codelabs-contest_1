/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.study.family.model;

import android.content.Context;
import android.util.Log;

import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.CloudDBZoneObjectList;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.ListenerHandler;
import com.huawei.agconnect.cloud.database.OnSnapshotListener;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Proxying implementation of CloudDBZone.
 */
public class CloudDBZoneWrapper {
    private static final String TAG = "CloudDBZoneWrapper";

    private AGConnectCloudDB mCloudDB;

    private CloudDBZone mCloudDBZone;

    private ListenerHandler mRegister;

    private CloudDBZoneConfig mConfig;

    private UiCallBack mUiCallBack = UiCallBack.DEFAULT;

    /**
     * Mark max id of family info. id is the primary key of {@link com.study.family.model.FamilyInfo}, so we must provide an value for it
     * when upserting to database.
     */
    private int mFamilyIndex = 0;

    private ReadWriteLock mReadWriteLock = new ReentrantReadWriteLock();

    /**
     * Monitor data change from database. Update family info list if data have changed
     */
    private OnSnapshotListener<com.study.family.model.FamilyInfo> mSnapshotListener = new OnSnapshotListener<com.study.family.model.FamilyInfo>() {
        @Override
        public void onSnapshot(CloudDBZoneSnapshot<com.study.family.model.FamilyInfo> cloudDBZoneSnapshot, AGConnectCloudDBException e) {
            if (e != null) {
                Log.w(TAG, "onSnapshot: " + e.getMessage());
                return;
            }
            CloudDBZoneObjectList<com.study.family.model.FamilyInfo> snapshotObjects = cloudDBZoneSnapshot.getSnapshotObjects();
            List<com.study.family.model.FamilyInfo> familyInfoList = new ArrayList<>();
            try {
                if (snapshotObjects != null) {
                    while (snapshotObjects.hasNext()) {
                        com.study.family.model.FamilyInfo familyInfo = snapshotObjects.next();
                        familyInfoList.add(familyInfo);
                        updateFamilyIndex(familyInfo);
                    }
                }
                mUiCallBack.onSubscribe(familyInfoList);
            } catch (AGConnectCloudDBException snapshotException) {
                Log.w(TAG, "onSnapshot:(getObject) " + snapshotException.getMessage());
            } finally {
                cloudDBZoneSnapshot.release();
            }
        }
    };

    public CloudDBZoneWrapper() {
       mCloudDB=AGConnectCloudDB.getInstance();
    }

    /**
     * Init AGConnectCloudDB in Application
     *
     * @param context application context
     */
    public static void initAGConnectCloudDB(Context context) {
        AGConnectCloudDB.initialize(context);
    }

    /**
     * Call AGConnectCloudDB.createObjectType to init schema
     */
    public void createObjectType() {
        try {
            mCloudDB.createObjectType(ObjectTypeInfoHelper.getObjectTypeInfo());
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "createObjectType: " + e.getMessage());
        }
    }

    /**
     * Call AGConnectCloudDB.openCloudDBZone to open a cloudDBZone.
     * We set it with cloud cache mode, and data can be store in local storage
     */
    public void openCloudDBZone() {
        mConfig = new CloudDBZoneConfig("QuickStartDemo",
                CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE,
                CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC);
        mConfig.setPersistenceEnabled(true);
        try {
            mCloudDBZone = mCloudDB.openCloudDBZone(mConfig, true);
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "openCloudDBZone: " + e.getMessage());
        }
    }

    public void openCloudDBZoneV2() {
        mConfig = new CloudDBZoneConfig("QuickStartDemo",
            CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE,
            CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC);
        mConfig.setPersistenceEnabled(true);
        Task<CloudDBZone> openDBZoneTask = mCloudDB.openCloudDBZone2(mConfig, true);
        openDBZoneTask.addOnSuccessListener(new OnSuccessListener<CloudDBZone>() {
            @Override
            public void onSuccess(CloudDBZone cloudDBZone) {
                Log.i(TAG, "Open cloudDBZone success");
                mCloudDBZone = cloudDBZone;
                // Add subscription after opening cloudDBZone success
                addSubscription();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w(TAG, "Open cloudDBZone failed for " + e.getMessage());
            }
        });
    }

    /**
     * Call AGConnectCloudDB.closeCloudDBZone
     */
    public void closeCloudDBZone() {
        try {
            mRegister.remove();
            mCloudDB.closeCloudDBZone(mCloudDBZone);
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "closeCloudDBZone: " + e.getMessage());
        }
    }

    /**
     * Call AGConnectCloudDB.deleteCloudDBZone
     */
    public void deleteCloudDBZone() {
        try {
            mCloudDB.deleteCloudDBZone(mConfig.getCloudDBZoneName());
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "deleteCloudDBZone: " + e.getMessage());
        }
    }

    /**
     * Add a callback to update family info list
     *
     * @param uiCallBack callback to update family list
     */
    public void addCallBacks(UiCallBack uiCallBack) {
        mUiCallBack = uiCallBack;
    }

    /**
     * Add mSnapshotListener to monitor data changes from storage
     */
    public void addSubscription() {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        try {
            CloudDBZoneQuery<com.study.family.model.FamilyInfo> snapshotQuery = CloudDBZoneQuery.where(com.study.family.model.FamilyInfo.class)
                    .equalTo(FamilyEditFields.SHADOW_FLAG, true);
            mRegister = mCloudDBZone.subscribeSnapshot(snapshotQuery,
                    CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY, mSnapshotListener);
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "subscribeSnapshot: " + e.getMessage());
        }
    }

    /**
     * Query all familys in storage from cloud side with CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY
     */
    public void queryAllFamilys() {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }
        Task<CloudDBZoneSnapshot<com.study.family.model.FamilyInfo>> queryTask = mCloudDBZone.executeQuery(
                CloudDBZoneQuery.where(com.study.family.model.FamilyInfo.class),
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<com.study.family.model.FamilyInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<com.study.family.model.FamilyInfo> snapshot) {
                processQueryResult(snapshot);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                mUiCallBack.updateUiOnError("Query family list from cloud failed");
            }
        });
    }

    /**
     * Query familys with condition
     *
     * @param query query condition
     */
    public void queryFamilys(CloudDBZoneQuery<com.study.family.model.FamilyInfo> query) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        Task<CloudDBZoneSnapshot<com.study.family.model.FamilyInfo>> queryTask = mCloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<com.study.family.model.FamilyInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<com.study.family.model.FamilyInfo> snapshot) {
                processQueryResult(snapshot);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                mUiCallBack.updateUiOnError("Query failed");
            }
        });
    }

    private void processQueryResult(CloudDBZoneSnapshot<com.study.family.model.FamilyInfo> snapshot) {
        CloudDBZoneObjectList<com.study.family.model.FamilyInfo> familyInfoCursor = snapshot.getSnapshotObjects();
        List<com.study.family.model.FamilyInfo> familyInfoList = new ArrayList<>();
        try {
            while (familyInfoCursor.hasNext()) {
                com.study.family.model.FamilyInfo familyInfo = familyInfoCursor.next();
                familyInfoList.add(familyInfo);
            }
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "processQueryResult: " + e.getMessage());
        } finally {
            snapshot.release();
        }
        mUiCallBack.onAddOrQuery(familyInfoList);
    }

    /**
     * Upsert familyinfo
     *
     * @param familyInfo familyinfo added or modified from local
     */
    public void upsertFamilyInfos(com.study.family.model.FamilyInfo familyInfo) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        Task<Integer> upsertTask = mCloudDBZone.executeUpsert(familyInfo);
        upsertTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
            @Override
            public void onSuccess(Integer cloudDBZoneResult) {
                Log.i(TAG, "Upsert " + cloudDBZoneResult + " records");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                mUiCallBack.updateUiOnError("Insert family info failed");
            }
        });
    }

    /**
     * Delete familyinfo
     *
     * @param familyInfoList familys selected by user
     */
    public void deleteFamilyInfos(List<com.study.family.model.FamilyInfo> familyInfoList) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        Task<Integer> deleteTask = mCloudDBZone.executeDelete(familyInfoList);
        if (deleteTask.getException() != null) {
            mUiCallBack.updateUiOnError("Delete family info failed");
            return;
        }
        mUiCallBack.onDelete(familyInfoList);
    }

    private void updateFamilyIndex(com.study.family.model.FamilyInfo familyInfo) {
        try {
            mReadWriteLock.writeLock().lock();
            if (mFamilyIndex < familyInfo.getID()) {
                mFamilyIndex = familyInfo.getID();
            }
        } finally {
            mReadWriteLock.writeLock().unlock();
        }
    }

    /**
     * Get max id of familyinfos
     *
     * @return max family info id
     */
    public int getFamilyIndex() {
        try {
            mReadWriteLock.readLock().lock();
            return mFamilyIndex;
        } finally {
            mReadWriteLock.readLock().unlock();
        }
    }

    /**
     * Call back to update ui in HomePageFragment
     */
    public interface UiCallBack {
        UiCallBack DEFAULT = new UiCallBack() {
            @Override
            public void onAddOrQuery(List<com.study.family.model.FamilyInfo> familyInfoList) {
                Log.i(TAG, "Using default onAddOrQuery");
            }

            @Override
            public void onSubscribe(List<com.study.family.model.FamilyInfo> familyInfoList) {
                Log.i(TAG, "Using default onSubscribe");
            }

            @Override
            public void onDelete(List<com.study.family.model.FamilyInfo> familyInfoList) {
                Log.i(TAG, "Using default onDelete");
            }

            @Override
            public void updateUiOnError(String errorMessage) {
                Log.i(TAG, "Using default updateUiOnError");
            }
        };

        void onAddOrQuery(List<com.study.family.model.FamilyInfo> familyInfoList);

        void onSubscribe(List<com.study.family.model.FamilyInfo> familyInfoList);

        void onDelete(List<com.study.family.model.FamilyInfo> familyInfoList);

        void updateUiOnError(String errorMessage);
    }
}
