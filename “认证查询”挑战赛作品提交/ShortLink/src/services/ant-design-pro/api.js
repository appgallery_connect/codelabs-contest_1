// @ts-ignore

/* eslint-disable */
import { request } from 'umi';
/** 获取当前的用户 GET /api/currentUser */
import * as agc from '../../agc';

export async function currentUser(options) {
  return agc.getUserInfo()
}
/** 退出登录接口 POST /api/login/outLogin */

export async function outLogin(options) {
  return agc.outLogin()
}
/** 登录接口 POST /api/login/account */

export async function login(body, options) {
  return agc.login(body)
}
/** 此处后端没有提供注释 GET /api/notices */

export async function getNotices(options) {
  return request('/api/notices', {
    method: 'GET',
    ...(options || {}),
  });
}

export async function rule(params, options) {
  return request('/api/rule', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}
/** 获取规则列表 GET /api/rule */
export async function urllist(params, options) {
  return agc.querySelf(params)
}
/** 新建规则 PUT /api/rule */

export async function updateRule(options) {
  return request('/api/rule', {
    method: 'PUT',
    ...(options || {}),
  });
}
/** 新建规则 POST /api/rule */

export async function addRule(options) {
  return agc.addUrl(options)
}
/** 删除规则 DELETE /api/rule */

export async function removeRule(options) {
  return agc.del(options)
}
