import {
    AGConnectCloudDB,
    CloudDBZoneQuery,
    CloudDBZoneConfig,
} from '@agconnect/database';

import { agconnect, agConnectConfig } from "./store";

let agcCloudDB, cloudDBZone
function initialize(schema) {
    if (cloudDBZone) agcCloudDB.closeCloudDBZone(cloudDBZone)
    AGConnectCloudDB.initialize(agConnectConfig)
    agcCloudDB = AGConnectCloudDB.getInstance()
    agcCloudDB.createObjectType(schema)
}
async function open(schema, cloudDBZoneName) {
    initialize(schema)
    cloudDBZone = await agcCloudDB.openCloudDBZone(new CloudDBZoneConfig(cloudDBZoneName))
}
/*
export declare class CloudDBZoneQuery<T> {
    private readonly queryConditions;
    private readonly clazz;
    private tableName?;
    private constructor();
    static where<T>(clazz: new () => T): CloudDBZoneQuery<T>;
    addCondition(conditionType: ConditionType | AggregateType, fieldName: string | undefined, value?: any | any[]): this;
    equalTo(fieldName: string, value: any): this;
    count(fieldName: string): this;
    beginsWith(fieldName: string, value: any): this;
    endsWith(fieldName: string, value: any): this;
    contains(fieldName: string, value: any): this;
    notEqualTo(fieldName: string, value: any): this;
    greaterThan(fieldName: string, value: any): this;
    greaterThanOrEqualTo(fieldName: string, value: any): this;
    lessThan(fieldName: string, value: any): this;
    lessThanOrEqualTo(fieldName: string, value: any): this;
    in(fieldName: string, values: any[]): this;
    isNull(fieldName: string): this;
    isNotNull(fieldName: string): this;
    orderByAsc(fieldName: string): this;
    orderByDesc(fieldName: string): this;
    limit(count: number, offset?: number): this;
    getClassName(): string;
    getClazz(): (new () => T) | undefined;
    getQueryConditions(): Condition[];
    private checkFieldValidity;
}
*/
async function query(clazz, callback) {
    const query = CloudDBZoneQuery.where(clazz);
    if (callback) callback(query)
    let snapshot = await cloudDBZone.executeQuery(query)
    return snapshot.getSnapshotObjects()
}
async function exec(obj) {
    return cloudDBZone.executeUpsert(obj)
}
async function del(obj) {
    return cloudDBZone.executeDelete(obj)
}
export {
    open,
    query,
    exec,
    del
}