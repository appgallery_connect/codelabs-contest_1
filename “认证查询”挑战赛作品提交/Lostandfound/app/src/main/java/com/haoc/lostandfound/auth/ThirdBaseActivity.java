package com.haoc.lostandfound.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.haoc.lostandfound.activity.MainActivity;
import com.haoc.lostandfound.R;
import com.huawei.agconnect.auth.AGConnectAuth;

public abstract class ThirdBaseActivity extends Activity {

    protected AGConnectAuth auth;
    private boolean link;

    private Button loginBtn;
    private Button linkBtn;
    private Button unlinkBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_base);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        link = getIntent().getBooleanExtra("link", false);
        auth = AGConnectAuth.getInstance();

        loginBtn = findViewById(R.id.btn_third_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        linkBtn = findViewById(R.id.link);
        linkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link();
            }
        });

        unlinkBtn = findViewById(R.id.unlink);
        unlinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unlink();
            }
        });

        if (link) {
            loginBtn.setVisibility(View.INVISIBLE);
        } else {
            linkBtn.setVisibility(View.INVISIBLE);
            unlinkBtn.setVisibility(View.INVISIBLE);
        }
    }

    public abstract void login();

    public abstract void link();

    public abstract void unlink();

    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    protected void loginSuccess() {
        startActivity(new Intent(this, MainActivity.class));
        LoginActivity.loginActivity.finish();
        finish();
    }
}