/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.haoc.lostandfound.clouddb;

import android.content.Context;
import android.util.Log;

import com.haoc.lostandfound.model.ObjectTypeInfoHelper;
import com.haoc.lostandfound.model.ThingInfo;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.CloudDBZoneObjectList;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.ListenerHandler;
import com.huawei.agconnect.cloud.database.OnSnapshotListener;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Proxying implementation of CloudDBZone.
 */
public class CloudDBZoneWrapper {
    private static final String TAG = "CloudDBZoneWrapper";

    private AGConnectCloudDB mCloudDB;

    private CloudDBZone mCloudDBZone;

    private ListenerHandler mRegister;

    private CloudDBZoneConfig mConfig;

    private UiCallBack mUiCallBack = UiCallBack.DEFAULT;

    /**
     * Mark max id of thing info. id is the primary key of {@link ThingInfo}, so we must provide an value for it
     * when upserting to database.
     */
    private int mThingIndex = 0;

    private ReadWriteLock mReadWriteLock = new ReentrantReadWriteLock();

    /**
     * Monitor data change from database. Update thing info list if data have changed
     */
    private OnSnapshotListener<ThingInfo> mSnapshotListener = new OnSnapshotListener<ThingInfo>() {
        @Override
        public void onSnapshot(CloudDBZoneSnapshot<ThingInfo> cloudDBZoneSnapshot, AGConnectCloudDBException e) {
            if (e != null) {
                Log.w(TAG, "onSnapshot: " + e.getMessage());
                return;
            }
            CloudDBZoneObjectList<ThingInfo> snapshotObjects = cloudDBZoneSnapshot.getSnapshotObjects();
            List<ThingInfo> ThingInfoList = new ArrayList<>();
            try {
                if (snapshotObjects != null) {
                    while (snapshotObjects.hasNext()) {
                        ThingInfo ThingInfo = snapshotObjects.next();
                        ThingInfoList.add(ThingInfo);
                        updateThingIndex(ThingInfo);
                    }
                }
                mUiCallBack.onSubscribe(ThingInfoList);
            } catch (AGConnectCloudDBException snapshotException) {
                Log.w(TAG, "onSnapshot:(getObject) " + snapshotException.getMessage());
            } finally {
                cloudDBZoneSnapshot.release();
            }
        }
    };

    public CloudDBZoneWrapper() {
        mCloudDB = AGConnectCloudDB.getInstance();
    }

    /**
     * Init AGConnectCloudDB in Application
     *
     * @param context application context
     */
    public static void initAGConnectCloudDB(Context context) {
        AGConnectCloudDB.initialize(context);
    }

    /**
     * Call AGConnectCloudDB.createObjectType to init schema
     */
    public void createObjectType() {
        try {
            mCloudDB.createObjectType(ObjectTypeInfoHelper.getObjectTypeInfo());
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "createObjectType: " + e.getMessage());
        }
    }

    /**
     * Call AGConnectCloudDB.openCloudDBZone to open a cloudDBZone.
     * We set it with cloud cache mode, and data can be store in local storage
     */
    public void openCloudDBZoneV2() {
        mConfig = new CloudDBZoneConfig("LostAndFound",
                CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE,
                CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC);
        //设置端侧Cloud DB zone的数据持久化
        mConfig.setPersistenceEnabled(true);
        Task<CloudDBZone> openDBZoneTask = mCloudDB.openCloudDBZone2(mConfig, true);
        openDBZoneTask.addOnSuccessListener(new OnSuccessListener<CloudDBZone>() {
            @Override
            public void onSuccess(CloudDBZone cloudDBZone) {
                Log.i(TAG, "Open cloudDBZone success");
                mCloudDBZone = cloudDBZone;
                // Add subscription after opening cloudDBZone success
                addSubscription();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w(TAG, "Open cloudDBZone failed for " + e.getMessage());
            }
        });
    }

    /**
     * Call AGConnectCloudDB.closeCloudDBZone
     */
    public void closeCloudDBZone() {
        try {
            mRegister.remove();
            mCloudDB.closeCloudDBZone(mCloudDBZone);
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "closeCloudDBZone: " + e.getMessage());
        }
    }

    /**
     * Call AGConnectCloudDB.deleteCloudDBZone
     */
    public void deleteCloudDBZone() {
        try {
            mCloudDB.deleteCloudDBZone(mConfig.getCloudDBZoneName());
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "deleteCloudDBZone: " + e.getMessage());
        }
    }

    /**
     * Add a callback to update thing info list
     *
     * @param uiCallBack callback to update thing list
     */
    public void addCallBacks(UiCallBack uiCallBack) {
        mUiCallBack = uiCallBack;
    }

    /**
     * Add mSnapshotListener to monitor data changes from storage
     */
    public void addSubscription() {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        try {
            CloudDBZoneQuery<ThingInfo> snapshotQuery = CloudDBZoneQuery.where(ThingInfo.class)
                    .equalTo(ThingEditFields.SHADOW_FLAG, true);
            mRegister = mCloudDBZone.subscribeSnapshot(snapshotQuery,
                    CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY, mSnapshotListener);
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "subscribeSnapshot: " + e.getMessage());
        }
    }

    /**
     * Query all things in storage from cloud side with CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY
     */
    public void queryAllThings() {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }
        Task<CloudDBZoneSnapshot<ThingInfo>> queryTask = mCloudDBZone.executeQuery(
                CloudDBZoneQuery.where(ThingInfo.class),
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<ThingInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<ThingInfo> snapshot) {
                processQueryResult(snapshot);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                mUiCallBack.updateUiOnError("Query thing list from cloud failed");
            }
        });
    }

    /**
     * Query things with condition
     *
     * @param query query condition
     */
    public void queryThings(CloudDBZoneQuery<ThingInfo> query) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        Task<CloudDBZoneSnapshot<ThingInfo>> queryTask = mCloudDBZone.executeQuery(query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(new OnSuccessListener<CloudDBZoneSnapshot<ThingInfo>>() {
            @Override
            public void onSuccess(CloudDBZoneSnapshot<ThingInfo> snapshot) {
                processQueryResult(snapshot);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                mUiCallBack.updateUiOnError("Query failed");
            }
        });
    }

    private void processQueryResult(CloudDBZoneSnapshot<ThingInfo> snapshot) {
        CloudDBZoneObjectList<ThingInfo> ThingInfoCursor = snapshot.getSnapshotObjects();
        List<ThingInfo> ThingInfoList = new ArrayList<>();
        try {
            while (ThingInfoCursor.hasNext()) {
                ThingInfo ThingInfo = ThingInfoCursor.next();
                ThingInfoList.add(ThingInfo);
            }
        } catch (AGConnectCloudDBException e) {
            Log.w(TAG, "processQueryResult: " + e.getMessage());
        } finally {
            snapshot.release();
        }
        mUiCallBack.onAddOrQuery(ThingInfoList);
    }

    /**
     * Upsert ThingInfo
     *
     * @param ThingInfo ThingInfo added or modified from local
     */
    public void upsertThingInfos(ThingInfo ThingInfo) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        Task<Integer> upsertTask = mCloudDBZone.executeUpsert(ThingInfo);
        upsertTask.addOnSuccessListener(new OnSuccessListener<Integer>() {
            @Override
            public void onSuccess(Integer cloudDBZoneResult) {
                Log.i(TAG, "Upsert " + cloudDBZoneResult + " records");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                mUiCallBack.updateUiOnError("Insert thing info failed");
            }
        });
    }

    /**
     * Delete ThingInfo
     *
     * @param ThingInfoList things selected by user
     */
    public void deleteThingInfos(List<ThingInfo> ThingInfoList) {
        if (mCloudDBZone == null) {
            Log.w(TAG, "CloudDBZone is null, try re-open it");
            return;
        }

        Task<Integer> deleteTask = mCloudDBZone.executeDelete(ThingInfoList);
        if (deleteTask.getException() != null) {
            mUiCallBack.updateUiOnError("Delete thing info failed");
            return;
        }
        mUiCallBack.onDelete(ThingInfoList);
    }

    private void updateThingIndex(ThingInfo ThingInfo) {
        try {
            mReadWriteLock.writeLock().lock();
            if (mThingIndex < ThingInfo.getId()) {
                mThingIndex = ThingInfo.getId();
            }
        } finally {
            mReadWriteLock.writeLock().unlock();
        }
    }

    /**
     * Get max id of ThingInfos
     *
     * @return max thing info id
     */
    public int getThingIndex() {
        try {
            mReadWriteLock.readLock().lock();
            return mThingIndex;
        } finally {
            mReadWriteLock.readLock().unlock();
        }
    }

    /**
     * Call back to update ui in HomePageFragment
     */
    public interface UiCallBack {
        UiCallBack DEFAULT = new UiCallBack() {
            @Override
            public void onAddOrQuery(List<ThingInfo> ThingInfoList) {
                Log.i(TAG, "Using default onAddOrQuery");
            }

            @Override
            public void onSubscribe(List<ThingInfo> ThingInfoList) {
                Log.i(TAG, "Using default onSubscribe");
            }

            @Override
            public void onDelete(List<ThingInfo> ThingInfoList) {
                Log.i(TAG, "Using default onDelete");
            }

            @Override
            public void updateUiOnError(String errorMessage) {
                Log.i(TAG, "Using default updateUiOnError");
            }
        };

        void onAddOrQuery(List<ThingInfo> ThingInfoList);

        void onSubscribe(List<ThingInfo> ThingInfoList);

        void onDelete(List<ThingInfo> ThingInfoList);

        void updateUiOnError(String errorMessage);
    }
}
