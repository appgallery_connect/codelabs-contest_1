package com.haoc.lostandfound.auth;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.haoc.lostandfound.R;
import com.haoc.lostandfound.utils.Myutils;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.TaskExecutors;

public class SettingsActivity extends Activity {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    private LoginActivity.Type type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        initView();
    }

    private void initView() {
        findViewById(R.id.layout_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 删除用户
                AGConnectAuth.getInstance().deleteUser()
                        .addOnSuccessListener(TaskExecutors.uiThread(), new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(SettingsActivity.this, "删除用户成功", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                                SettingsActivity.this.finish();
                            }
                        }).addOnFailureListener(TaskExecutors.uiThread(), new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Toast.makeText(SettingsActivity.this, "删除用户失败:" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        findViewById(R.id.imgView_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.layout_update_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEmail();
            }
        });

        findViewById(R.id.layout_update_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePhone();
            }
        });

        findViewById(R.id.layout_update_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });

        findViewById(R.id.layout_reset_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });
    }

    // 修改手机号码
    private void updatePhone() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_phone, null, false);
        final AlertDialog dialog = new AlertDialog.Builder(this).setView(view).create();
        EditText editAccount = view.findViewById(R.id.et_account);
        EditText editCode = view.findViewById(R.id.et_verify_code);
        Button send = view.findViewById(R.id.btn_send);
        // 发送验证码
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editAccount.getText().toString().trim();
                Myutils.sendVerificationCodeByPhoneNumber(SettingsActivity.this, send, phoneNumber, VerifyCodeSettings.ACTION_REGISTER_LOGIN);
            }
        });
        Button update = view.findViewById(R.id.update);
        // 更新手机号码
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editAccount.getText().toString().trim();
                String verifyCode = editCode.getText().toString().trim();
                Myutils.updatePhone(SettingsActivity.this, phoneNumber, verifyCode);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // 修改邮箱地址
    private void updateEmail() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_email, null, false);
        final AlertDialog dialog = new AlertDialog.Builder(this).setView(view).create();
        EditText editAccount = view.findViewById(R.id.et_account);
        EditText editCode = view.findViewById(R.id.et_verify_code);
        Button send = view.findViewById(R.id.btn_send);
        // 发送验证码
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editAccount.getText().toString().trim();
                Myutils.sendVerificationCodeByEmail(SettingsActivity.this, send, email, VerifyCodeSettings.ACTION_REGISTER_LOGIN);
            }
        });
        Button update = view.findViewById(R.id.update);
        // 更新邮箱地址
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editAccount.getText().toString().trim();
                String verifyCode = editCode.getText().toString().trim();
                Myutils.updateEmail(SettingsActivity.this, email, verifyCode);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // 修改密码
    private void updatePassword() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_update_password, null, false);
        final AlertDialog dialog = new AlertDialog.Builder(this).setView(view).create();
        EditText editAccount = view.findViewById(R.id.et_account);
        EditText editNewPassword = view.findViewById(R.id.et_new_password);
        EditText editCode = view.findViewById(R.id.et_verify_code);
        Button send = view.findViewById(R.id.btn_send);
        AGConnectUser agConnectUser = AGConnectAuth.getInstance().getCurrentUser();
        // 获取当前登录的帐号
        if (type == LoginActivity.Type.EMAIL) {
            String email = agConnectUser.getEmail();
            editAccount.setText(email);
            // 发送验证码
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Myutils.sendVerificationCodeByEmail(SettingsActivity.this, send, email, VerifyCodeSettings.ACTION_RESET_PASSWORD);
                }
            });
            Button update = view.findViewById(R.id.update);
            // 更改密码
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newPassword = editNewPassword.getText().toString().trim();
                    String verifyCode = editCode.getText().toString().trim();
                    Myutils.updatePassword(SettingsActivity.this, newPassword, verifyCode, AGConnectAuthCredential.Email_Provider);
                    dialog.dismiss();
                }
            });
            dialog.show();
        } else {
            TextView tv_account = view.findViewById(R.id.tv_account);
            tv_account.setText(R.string.phone_number);
            String phoneNumber = agConnectUser.getPhone().substring(4);
            editAccount.setText(phoneNumber);
            // 发送验证码
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Myutils.sendVerificationCodeByPhoneNumber(SettingsActivity.this, send, phoneNumber, VerifyCodeSettings.ACTION_RESET_PASSWORD);
                }
            });
            Button update = view.findViewById(R.id.update);
            // 更改密码
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newPassword = editNewPassword.getText().toString().trim();
                    String verifyCode = editCode.getText().toString().trim();
                    Myutils.updatePassword(SettingsActivity.this, newPassword, verifyCode, AGConnectAuthCredential.Phone_Provider);
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    // 重置密码
    private void resetPassword() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_reset_password, null, false);
        final AlertDialog dialog = new AlertDialog.Builder(this).setView(view).create();
        EditText editAccount = view.findViewById(R.id.et_account);
        EditText editCode = view.findViewById(R.id.et_verify_code);
        Button send = view.findViewById(R.id.btn_send);
        AGConnectUser agConnectUser = AGConnectAuth.getInstance().getCurrentUser();
        // 获取当前登录的帐号
        if (type == LoginActivity.Type.EMAIL) {
            String email = agConnectUser.getEmail();
            editAccount.setText(email);
            // 发送验证码
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Myutils.sendVerificationCodeByPhoneNumber(SettingsActivity.this, send, email, VerifyCodeSettings.ACTION_RESET_PASSWORD);
                }
            });
            Button reset = view.findViewById(R.id.reset);
            // 重置密码
            reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String verifyCode = editCode.getText().toString().trim();
                    Myutils.resetEmailPassword(SettingsActivity.this, email, verifyCode);
                    dialog.dismiss();
                }
            });
            dialog.show();
        } else {
            TextView tv_account = view.findViewById(R.id.tv_account);
            tv_account.setText(R.string.phone_number);
            String phoneNumber = agConnectUser.getPhone().substring(4);
            editAccount.setText(phoneNumber);
            // 发送验证码
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Myutils.sendVerificationCodeByPhoneNumber(SettingsActivity.this, send, phoneNumber, VerifyCodeSettings.ACTION_RESET_PASSWORD);
                }
            });
            Button reset = view.findViewById(R.id.reset);
            // 重置密码
            reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String verifyCode = editCode.getText().toString().trim();
                    Myutils.resetPhonePassword(SettingsActivity.this, phoneNumber, verifyCode);
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
}