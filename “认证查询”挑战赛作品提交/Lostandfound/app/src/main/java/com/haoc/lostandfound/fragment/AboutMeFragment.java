/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.haoc.lostandfound.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.haoc.lostandfound.activity.AboutActivity;
import com.haoc.lostandfound.activity.MainActivity;
import com.haoc.lostandfound.R;
import com.haoc.lostandfound.auth.AuthMainActivity;
import com.haoc.lostandfound.auth.LoginActivity;
import com.huawei.agconnect.auth.AGConnectAuth;


public class AboutMeFragment extends Fragment {

    private View mLoginUserInfoView;

    private MainActivity mActivity;

    public static Fragment newInstance() {
        return new AboutMeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_me, container, false);
        initUserDetailView(rootView);
        return rootView;
    }

    private void initUserDetailView(View rootView) {
        mLoginUserInfoView = rootView.findViewById(R.id.login_user_info);

        View userAccountView = mLoginUserInfoView.findViewById(R.id.user_account);
        TextView userAccountTitleView = userAccountView.findViewById(R.id.title);
        userAccountTitleView.setText(R.string.user_account);
        userAccountView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AuthMainActivity.class));
            }
        });

        View aboutView = mLoginUserInfoView.findViewById(R.id.about);
        ImageView aboutImgView = aboutView.findViewById(R.id.img);
        aboutImgView.setImageResource(R.mipmap.details);
        TextView aboutTitleView = aboutView.findViewById(R.id.title);
        aboutTitleView.setText(R.string.about);
        aboutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AboutActivity.class));
            }
        });

        View logoutView = mLoginUserInfoView.findViewById(R.id.logout);
        logoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 登出
                AGConnectAuth.getInstance().signOut();
                startActivity(new Intent(getContext(), LoginActivity.class));
                getActivity().finish();
            }
        });
    }
}
