package com.haoc.lostandfound.utils;

import android.os.CountDownTimer;
import android.widget.Button;

/**
 * @description: 验证码倒计时工具类
 * @author: Haoc
 * @Time: 2022/4/24  11:55
 */

public class CountDownTimerUtils extends CountDownTimer {
    private Button timeButton;

    public CountDownTimerUtils(Button button, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
        this.timeButton = button;
    }

    //计时过程
    @Override
    public void onTick(long l) {
        //防止计时过程中重复点击
        timeButton.setClickable(false);
        timeButton.setText(l / 1000 + "秒后重新发送");
    }

    //计时完毕的方法
    @Override
    public void onFinish() {
        //重新给Button设置文字
        timeButton.setText("重新获取");
        //设置可点击
        timeButton.setClickable(true);
    }
}
